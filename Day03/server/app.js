console.log("Day Two Exercises");
var express = require("express");
var app = express();
console.log(__dirname);
console.log(__dirname + "/../");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));
app.listen(NODE_PORT,function(){
    console.log("Web App Started at localhost:" + NODE_PORT);
});
